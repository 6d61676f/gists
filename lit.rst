#############
 Author List
#############

-  Pietro Bembo — 1470 – 1547

   -  [X] Gli Asolanii

-  Machiavelli — 1469 – 1527

   -  [X] The Prince
   -  [X] Mandragola
   -  [X] Clizia

-  Lorenzo Valla — 1407 – 1457

-  Boccaccio — 1313 – 1375

   -  [X] Decameron
   -  [X] Corbaccio
   -  *Il Filostrato*
   -  *Il Filocolo*
   -  **Genealogia Deorum Gentilium**

-  Petrarca — 1304 – 1374

   -  [X] Canzoniere
   -  *Trionfi*
   -  *De Vita Solitaria*
   -  *De Remediis Utriusque Fortunae*

-  Jean Buridan — 1301 – 1359/62

-  William of Ockham — 1287 – 1347

-  Duns Scotus — 1266 – 1308

   -  [X] Despre primul principiu

-  Dante — 1265 – 1321

   -  [X] Comedia

-  Gautier de Metz — XII – XIII

   -  *Imago Mundi*

-  Toma de Aquina — 1225 – 1274

   -  [X] Despre fiind si esenta

-  Chrétien de Troyes — 1135 – 1185

   -  [X] Four Arthurian Romances

-  Roger Bacon — 1219 – 1292

   -  *Opus Maius*

-  Albertus Magnus — 1200 – 1280

-  Averroes — 1126 – 1198

-  Peter Abelard — 1079 – 1142

   -  *Sic et Non*

-  Anselm de Canterbury — 1033 – 1109

   -  [X] Despre adevar
   -  Proslogion

-  Avicenna — 980 – 1037

-  Isidore of Seville — 560 – 636

   -  [X] Etimologii XI/XII

-  Procopius of Caesarea — 500 – 565?

   -  `Buildings
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Procopius/Buildings/home.html>`__
   -  `The Secret History
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Procopius/Anecdota/home.html>`__
   -  *History of the Wars*

-  Coluthus — V – VI?

   -  `The Rape of Helen
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Colluthus/Rape_of_Helen*.html>`__

-  Macrobius — V – V

   -  `Saturnalia
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Macrobius/Saturnalia/home.html>`__

-  Rutilius Namatianus — V – V

   -  `de Reditu suo
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Rutilius_Namatianus/text*.html>`__

-  Boethius — 480 – 525

   -  [X] De consolatione philosophiae

-  Proclus Lycaeus — 412 – 485

-  Julius Obsequens — IV – V

   -  `Book of Prodigies
      <http://www.attalus.org/translate/obsequens.html>`__

-  Tiberius Claudius Donatus — IV – V

-  Publius Flavius Vegetius — IV – IV

   -  *De re militari*
   -  *Digesta Artis Mulomedicinae*

-  Paulinus Pellaeus — 377 – 461?

   -  `Eucharisticos
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Paulinus_Pellaeus/Eucharisticus*.html>`__

-  Claudius Claudianus — 370 – 404

   -  `Works
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Claudian/home.html>`__

-  St. Augustine — 354 – 430

   -  [X] Confesiuni
   -  The City of God

-  Eusebius of Caesarea — 260 – 339

   -  *Works*

-  Lactantius — 250 – 325

   -  *Institutiile divine*

-  Iamblichus — 245 – 325

-  Porphyry of Tyre — 234 – 305

-  Plotinus — 204 – 270

   -  Complete Works (1 2 3 4) Vol

-  Aelius Donatus — III – III

-  Titus Calpurnius Siculus — III? – III

   -  `Eclogues
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Calpurnius_Siculus/home.html>`__

-  Marcus Aurelius Nemesianus — III – III

   -  `The Chase
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Nemesianus/Cynegetica*.html>`__
   -  `Eclogues
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Nemesianus/Eclogues/1*.html>`__
   -  `Fragments on Bird—Catching
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Nemesianus/de_Aucupio*.html>`__

-  Diogenes Laertius — III – III

   -  Lives and Opinions of Eminent Philosophers

-  Athenaeus of Naucratis — II – III

   -  *The Deipnosophistae*

-  Origen — 184 – 253

   -  Philocalia

-  Ammonius Saccas — 175 – 242

-  Claudius Aelianus — 175 – 235

   -  *De Natura Animalium*

-  Philostratus — 170 – 247

   -  *Life of Apollonius of Tyana*
   -  *Lives of Sophists*
   -  *Gymnasticus*
   -  *Heroicus*
   -  *Epistolae*
   -  *Imagines*

-  Artemidorus Daldianus — II – II

   -  *Oneirocritica*

-  Babrius — II – II

   -  *Greek Fables*

-  Aelianus Tacticus — II – II

   -  *On Tactical Arrays of the Greeks*

-  Sextus Empiricus — 160 – 210

-  Sextus Jullius Africanus — 160 – 240

   -  *Κέστοι "Embroidered"*
   -  *Chronographiai*

-  Tertullian — 155 – 240

   -  Complete Works

-  Cassius Dio — 155 – 235

   -  *Roman History*

-  Oppian — II – II

   -  `The Cynegetica and the Halieutica
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Oppian/home.html>`__

-  Athenaeus of Naucratis — II – III

   -  *Deipnosophistae*

-  Celsus — II – II

   -  *The True Word*

-  Polyaenus — II – II

   -  `Stratagems in War <http://www.attalus.org/info/polyaenus.html>`__

-  Aulus Gellius — 125 – 180?

   -  `Attic Nights
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Gellius/home.html>`__

-  Lucian din Samosata — 125 – 180

   -  [X] Scrieri alese

-  Apuleius — 124 – 170

   -  [X] The Golden Ass
   -  [X] Apologia

-  Marcus Aurelius — 121 – 180

   -  [X] Meditations

-  Pausanias — 110 – 180

   -  *Description of Greece*

-  Claudius Ptolemy — 100 – 170

   -  Tetrabiblos

-  Pseudo—Apollodorus – I - II ?

   -  Bibliotheca

-  Longinus — I – I

   -  `On the Sublime <http://attalus.org/translate/longinus.html>`__

-  Athenaeus of Attalia — I – I

-  Quintus Curtius Rufus — I – I

   -  `Histories of Alexander the Great
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Curtius/home.html>`__

-  Onasander — I – I

   -  `Strategikos
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Onasander/home.html>`__

-  Appian of Alexandria — 95 – 165

   -  Roman History and The Civil Wars
   -  The Foreign Wars

-  Arrian of Nicomedia — 86? – 146?

   -  *Extant Works*

-  Suetonius — 69 – 140

   -  [X] The Twelve Caesars

-  Lucius Annaeus Florus — 74 – 130

   -  `Epitome of Roman History
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Florus/Epitome/home.html>`__

-  Julius Florus — 74 – 130

-  Publius Annius Florus — ? – ?

   -  `Poems
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Florus/poems/home.html>`__

-  Pliny the Younger — 61 – 113

-  Tacitus — 56 – 120

   -  [X] Germany
   -  [X] Agricola

-  Epictetus — 55 – 135

   -  [X] Discourses
   -  [X] Enchiridion

-  Plutarch — 46 – 120

   -  [X] Lives of the noble Grecians and Romans
   -  [X] Moral Essays

-  Statius — 45 – 96

   -  [X] Thebaid

-  Sextus Julius Frontinus — 40 – 103

   -  `De aquaeductu
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Frontinus/De_Aquis/home.html>`__
   -  `Strategemata
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Frontinus/Strategemata/home.html>`__

-  Lucan — 39 – 65

   -  [X] Pharsalia

-  Martialis — 38 – 102

   -  [X] Epigrams

-  Quintilian — 35 – 100

   -  [X] Institutio Oratoria

-  Aulus Persius Flaccus — 34 – 62

   -  [X] Satires

-  Sulpicia the Satirist — ?

   -  [X] Satire

-  Juvenal — I – II?

   -  [X] Satire

-  Antoninus Liberalis — I? – III?

   -  *Metamorphoses*

-  Aufidius Bassus — I

-  Italicus Silius — 28 – 103

   -  [X] Punica

-  Petronius Arbiter — 27 – 66

   -  [X] Satyricon

-  Pliny the Elder — 24 – 79

-  Lucius Junius Columella — 4 – 70

   -  *De de rustica*
   -  *De arborirus*

-  Publius Pomponius Secundus — I – I AD

-  Grattius Faliscus — I BC – I AD

   -  `Cynegeticon
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Grattius/home.html>`__

-  Seneca — 4 BC – 65

   -  [X] Tragedii

      -  Oedipus
      -  Thyestes
      -  Agamemnon Phaedra
      -  Hercule scos din minti
      -  Troienele
      -  Hercule pe muntele Oeta
      -  Fenicienele

   -  [X] Dialogues

      -  To Marcia (On Consolation)
      -  On Anger
      -  To Helvia (On Consolation)
      -  To Polybius (On Consolation)
      -  On The Shortness Of Life,
      -  On Leisure
      -  On Peace Of Mind
      -  On Providence
      -  On The Firmness of The Wise Person
      -  On A Happy Life, On Clemency
      -  On Benefits

-  Gaius Julius Phaedrus — 15BC – 50

   -  [X] Fables

-  Marcus Velleius Paterculus — 19BC – 31

   -  `The Roman History
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Velleius_Paterculus/home.html>`__

-  Aulus Cornelius Celsus — 25 BC – 50

   -  `De Medicina
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Celsus/home.html>`__

-  Ovid — 43 BC – 17

   -  [X] Opere

      -  Amoruri
      -  Heroidele
      -  Arta iubirii
      -  Remediile iubirii
      -  Cosmetice
      -  Metamorfoze
      -  Tristele
      -  Ponticele

-  Propertius — 50 – 15BC

   -  [X] The Love Elegies

-  Tibullus — 55 BC – 19

   -  [X] Poems (Delia, Nemesis)
   -  [X] Sulpicia's Garland
   -  [X] Sulpicia's Verses

-  Titus Livius — 62/59 BC – 17/12 AD

   -  [X] From the Founding of the City

-  Horatius — 63 – 8 BC

   -  [X] Ode
   -  [X] Epode
   -  [X] Carmen Saeculare
   -  [X] Satire
   -  [X] Epistole
   -  [X] Arta Poetica

-  Strabo — 64 BC – 24 AD

   -  Geographica

-  Nicolaus of Damascus — 64 BC – ?

-  Gaius Julius Hyginus — 64 BC – 17 AD

   -  [X] De Astronomica
   -  Fabulae

-  Valerius Maximus — I – I AD

   -  `Nine books of memorable deeds and sayings
      <http://www.attalus.org/info/valerius.html>`__

-  Virgil — 68 – 19 BC

   -  [X] Bucolicele

   -  [X] Georgicele

   -  [X] Eneida

   -  [X] Appendix Vergiliana

      -  Crasmarita
      -  Turta
      -  Ramas bun de la studiile de mai inainte
      -  Despre Sabin
      -  Catre Venus
      -  Inscriptii

-  Gaius Asinius Pollio — 75 BC – 4 AD

-  Vitruvius — 78/70 – 15 BC

   -  [X] On Architecture

-  Catullus — 84 – 5 4BC

   -  [X] Poems

-  Aemilius Macer — ? – 16 BC

-  Gaius Sallustius Crispus — 86 – 35 BC

   -  [X] The War with Catiline

   -  [X] The War with Jugurtha

   -  [X] Orations and Letters from The Histories

      -  Speech of the consul Lepidus to the Roman People
      -  The speech of Philippus in the Senate
      -  Speech of Gaius Cotta to the Roman People
      -  Letter of Gnaeus Pompeius to the Senate
      -  The speech of Macer, tribune of the Commons to the Commons
      -  The letter of Mithridates

   -  [X] Pseudo—Sallustian Works

      -  Speech on the state, addressed to Caesar in his later years
      -  Letter to Caesar on the state
      -  An Invective against Marcus Tullius
      -  An Invective against Sallustius Crispus

-  Diodorus Siculus — 90 – 30 BC

   -  *Bibliotheca historica*

-  Publius Nigidius Figulus — 98 – 45 BC

-  Titus Lucretius Carus — 99 – 55 BC

   -  [X] De rerum natura

-  Aenesidemus — I – I BC

-  Catius — I – I BC

-  Athenaeus Mechanicus — I – I BC

-  Julius Caesar — 100 – 44 BC

   -  [X] The Gallic War
   -  [X] The Civil War

-  Cicero — 106 – 43 BC

   -  [X] Complete Works

      -  Orations
      -  Rhetorical Treatises
      -  Political Treatises
      -  Philosophical Treatises
      -  Poetry
      -  Rhetorica ad Herennium
      -  Commentatiolum Petitionis

   -  [X] Selected Letters

-  Diodorus of Tyre — I – I BC

-  Asclepiodotus Tacticus — I – I BC

   -  `Tactics
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Asclepiodotus/home.html>`__

-  Procilius — I – I BC

-  Hostilius Saserna — I BC?

-  Cornelius Nepos — 110 – 25 BC

   -  [X] Lives of eminent commanders

-  Marcus Terentius Varro — 112 – 27 BC

   -  [X] On Agriculture
   -  [X] On The Latin Language

-  Lucius Cornelius Sisenna — 120 – 67 BC

-  Antiochus of Ascalon — 125 – 68 BC

-  Posidonius — 135 – 51 BC

-  Phaedrus the Epicurean — 138 – 70 BC

-  Philo of Larissa — 159 – 84 BC

-  Lucius Accius — 170 – 86 BC

-  Lucius Tarutius Firmanus — ? – 86 BC

-  Lucius Coelius Antipater — 180 – 120 BC

-  Gaius Lucilius — 180 – 103 BC

   -  [X] Fragments of Satire

-  Panaetius — 185 – 110 BC

-  Publius Terentius Afer — 195? – 159? BC

   -  [X] Fata din Andros
   -  [X] Eunucul
   -  [X] Heautontimorumenos
   -  [X] Adelphi
   -  [X] Hecyra
   -  [X] Phormio

-  Crates of Mallus — II – II BC

-  Gaius Blossius — II – II BC

-  Bion — II?

   -  [X] The six extant poems
   -  [X] Fragments

-  Moschus — II?

   -  [X] The nine extant idyls

-  Polybius — 200 – 118 BC

   -  [X] The Histories

-  Carneades — 214 – 129 BC

-  Marcus Pacuvius — 220 – 130 BC

-  Caecilius Statius — 220 – 166 BC

-  Cato the Elder — 234 – 149 BC

   -  [X] On Agriculture

-  Quintus Ennius — 239 – 169 BC

-  Titus Maccius Plautus — 254 – 184 BC

   -  [X] Complete works
   -  Soldatul fanfaron
   -  Ulcica

-  Sotades — III BC

-  Aristo of Ceos — III BC

-  Aristo of Chios — III BC

-  Apollonius of Rhodes — III BC

   -  [X] Peripețiile corăbiei Argo

-  Quintus Fabius Pictor — 270 – 200 BC

-  Eratosthenes of Cyrene — 276 – 195 BC

-  Chrysippus of Soli — 279 – 206 BC

-  Philo Mechanicus — 280 – 220 BC

   -  *Compendium of Mechanics*

-  Hieronymus of Rhodes — 290 – 230 BC

-  Theocritus — 300 – 260 BC

   -  [X] Idile
   -  [X] Complete works

-  Callimachus — 310/305 – 240 BC

   -  [X] Complete works

-  Aratus of Soli — 315 – 240 BC

   -  [X] Phaenomena

-  Arcesilaus — 316 – 241 BC

-  Asclepiades of Samos — 320 – ? BC

-  Timon of Phlius — 320 – 235 BC

-  Praxiphanes — IV – IV BC

-  Manetho — IV – III BC

   -  `Extant Works
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Manetho/home.html>`__

-  Cleanthes — 330 – 230 BC

   -  [X] The Hymn

-  Zeno of Citium — 334 – 262 BC

-  Diodorus Cronus — ? – 284 BC

-  Philitas of Cos — 340 – 285 BC

-  Epicurus — 341 – 270 BC

   -  [X] Essential Epicurus

      -  Vatican Sayings
      -  Letter to Menoeceus
      -  Letter to Herodotus
      -  Letter to Pythocles
      -  Letter to Idomeneus
      -  Last Will

-  Menander — 342 – 290 BC

   -  [X] Plays and Fragments

-  Pytheas of Massalia — 350 – III BC

-  Demetrius of Phalerum — 350 – 280 BC

   -  `On Style <http://www.attalus.org/info/demetrius.html>`__

-  Timaeus of Tauromenium — 355 – 260 BC

-  Demochares — 355 – 275 BC

-  Pyrrho of Elis — 360 – 270 BC

-  Callisthenes of Olynthus — 360 – 327 BC

-  Dinarchus — 361 – 291 BC

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0082>`__

-  Philemon — 362 – 262 BC

-  Crates of Thebes — 365 – 285 BC

-  Theophrastus — 371 – 287 BC

   -  [X] Inquiry into plants
   -  [X] On characters
   -  [X] Treatise on Odours
   -  [X] Concerning Weather Sings
   -  [X] On Winds
   -  [X] On stones

-  Demades — 380 – 318 BC

   -  [X] `On The Twelve Years
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0066>`__

-  Theopompus — 380 – 315 BC

-  Anaximenes of Lampsacus — 380 – 320 BC

   -  *Rhetorica ad Alexandrum*

-  Aristotel — 384 – 322 BC

   -  [X] Organon — Vol I

      -  Categorii
      -  Despre interpretare
      -  Analitica prima

   -  [X] Organon — Vol II

      -  Analitica secunda
      -  Topica
      -  Respingerile sofistice

   -  [X] Retorica

   -  [X] Poetica

   -  [X] Etica nicomahica

   -  [X] Politica

   -  [X] Fizica

   -  [X] On the Heavens

   -  [X] On Generation and Corruption

   -  [X] Meteorology

   -  [X] On the Soul

   -  [X] Parva naturalia

      -  [X] Despre simtire si cele sensibile
      -  [X] Despre memorie si reamintire
      -  [X] Despre somn si veghe
      -  [X] Despre vise
      -  [X] Despre profetia ivita in somn
      -  [X] Despre lungimea si scurtimea vietii
      -  [X] Despre tinere si batranete, viata si moarte

   -  [X] Metafizica

-  Demosthenes — 384 – 322 BC

   -  [X] Complete Works

-  Aeschines — 389 – 314 BC

   -  [X] Against Ctesiphon
   -  [X] Against Timarchus
   -  [X] On the False Embassy

-  Hypereides — 390 – 322 BC

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0140>`__

-  Lycurgus of Athens — 390 – 324 BC

   -  [X] `Against Leocrates
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0152>`__

-  Isaeus — V – IV BC?

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0142>`__

-  Leon of Pella — IV – IV BC

   -  [X] `Extant fragments
      <https://www.jasoncolavito.com/leon—of–pella.html>`__

-  Aeneas Tacticus — IV – IV BC

   -  [X] `How to survive under siege
      <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Aeneas_Tacticus/home.html>`__

-  Ephorus of Cyme — 400 – 330 BC

-  Eudoxus of Cnidus — 408 – 355 BC

-  Diogenes the Cynic — 414 – 323 BC

-  Plato — 428 – 348 BC

   -  [X] Apology
   -  [X] Crito
   -  [X] Euthyphro
   -  [X] Phaedo
   -  [X] Meno
   -  [X] Gorgias
   -  [X] Symposium
   -  [X] Republic
   -  [X] Phaedrus
   -  [X] Parmenides
   -  [X] Theaetetus
   -  [X] Protagoras
   -  [X] The Sophist
   -  [X] The Statesman
   -  [X] Timaeus
   -  Lysis
   -  Menexenus
   -  Hyppias Minor

-  Sophron — V – V BC

-  Xenophon — 431 – 354 BC

   -  [X] Dialogues

      -  Hiero the Tyrant
      -  Agesilaus
      -  Cavalry Commander
      -  Horsemanship
      -  Hunting
      -  Ways and Means

   -  [X] Conversations of Socrates

      -  Defence
      -  Memoirs
      -  The Dinner Party
      -  The Estate Manager

   -  [X] Cyrus the Great

   -  [X] A History of My Times

   -  [X] Anabasis of Cyrus

   -  Spartan Society

-  The Old Oligarch — V – V BC

   -  [X] Constitution of the Athenians

-  Aristippus — 435 – 365 BC

-  Isocrates — 436 – 338 BC

   -  [X] `Letters
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0246>`__
   -  [X] `Speeches
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0144>`__
   -  [X] `Cyprian Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.04.0089>`__

-  Andocides — 440 – 390 BC

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0018>`__

-  Euhumerus — IV – III BC

-  Lysias — 445 – 380 BC

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0154>`__

-  Eupolis — 446 – 411 BC

-  Aristophanes — 446 – 386 BC

   -  [X] Knights
   -  [X] Peace
   -  [X] Birds
   -  [X] Wealth
   -  [X] The Assemblywomen
   -  [X] The Acharnians
   -  [X] The Clouds
   -  [X] Lysistrata
   -  [X] Frogs
   -  [X] Women at the Thesmophoria
   -  [X] Wasps

-  Antisthenes — 446 – 366 BC

-  Thrasymachus of Chalcedon — 459 – 400 BC

   -  [X] `Fragments <https://www.sacred—texts.com/cla/app/app80.htm>`__

-  Democritus — 460 – 370 BC

-  Thucydides — 460 – 400 BC

   -  [X] The Peloponnesian War

-  Prodicus of Ceos — 465 – 395 BC

-  Socrates — 470 – 399 BC

-  Antiphon — 480 – 411 BC

   -  [X] `Extant Orations
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0020>`__

-  Euripides — 480 – 406 BC

   -  [X] Alcestis
   -  [X] Medea
   -  [X] Heracleidae
   -  [X] Hippolytus
   -  [X] Andromache
   -  [X] Hecuba
   -  [X] The Suppliants
   -  [X] Electra
   -  [X] Herakles
   -  [X] The Trojan Women
   -  [X] Iphigenia in Tauris
   -  [X] Ion
   -  [X] Helen
   -  [X] Phoenician Women
   -  [X] Orestes
   -  [X] Bacchae
   -  [X] Iphigenia in Aulis
   -  [X] Rhesus
   -  [X] Cyclops

-  Gorgias — 483 – 375 BC

-  Herodotus — 484 – 425 BC

   -  [X] Histories

-  Protagoras — 490 – 420 BC

-  Empedocles — 494 – 434 BC

-  Zeno of Elea — 495 – 430 BC

-  Sophocles — 497 – 406 BC

   -  [X] Antigone
   -  [X] Oedipus the King
   -  [X] Oedipus at Colonus
   -  [X] Ajax
   -  [X] The Women of Trachis
   -  [X] Electra
   -  [X] Philoctetes
   -  [X] The Trackers

-  Leucippus — V – V BC

-  Hippias of Elis — V – V BC

   -  [X] `Fragments <https://www.sacred—texts.com/cla/app/app81.htm>`__

-  Anaxagoras — 500 – 428 BC

-  Parmenides — 515 – ? BC

-  Cleostratus of Tenedos — 520 – 432 BC

-  Bacchylides — 518 – 451 BC

   -  [X] `Fragments of Epinicians and Dithyrambs
      <https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0064>`__

-  Pindar — 518 – 438 BC

   -  [X] Olympians
   -  [X] Pythians
   -  [X] Nemeans
   -  [X] Isthmians

-  Cratinus — 519 – 422 BC

-  Aeschylus — 525 – 456 BC

   -  [X] Orestia
   -  [X] Rugatoarele
   -  [X] Persii
   -  [X] Sapte contra Tebei
   -  [X] Prometeu Inlantuit

-  Heraclitus — 535 – 475 BC

-  Hecataeus of Miletus — 550 – 476 BC

-  Epicharmus of Kos — 550 – 460 BC

-  Charondas — VI – V BC

-  Pythagoras — 570 – 495 BC

-  Xenophanes — 570 – 475 BC

-  Pherecydes of Syros — 580 – 520 BC

-  Anaximenes — 586 – 526 BC

-  Theognis of Megara — VI – VI BC

-  Phocylides — VI – VI BC

-  Anaximander — 610 – 546 BC

-  Aesop — 620 – 564 BC

   -  [X] Fables

-  Thales — 624 – 548 BC

-  Stesichorus — 630 – 555 BC

-  Archilocus — 680 – 645 BC

-  Stasinus — VIII – VII BC

-  Hesiod — VIII – VII BC

   -  [X] Theogony
   -  [X] Works and Days
   -  [X] Shield of Heracles

-  Homer — VIII – VII BC

   -  [X] Iliad
   -  [X] Odyssey


######
 Misc
######

-  [X] Dies Irae — XIII

-  [X] Magna Carta — 1215

-  [X] Carmina Burana — XII

-  [X] Marele imn Homeric catre Afrodita — VII BC

-  *Suda* — X

-  Antologia palatina

-  Select Epigrams From the Greek Anthology

-  Love, Worship and Death: Some Renderings from the Greek Anthology

-  *Apicius a.k.a De re culinaria*

-  Robin Hard

   -  [X] Eratosthenes and Hyginus Constellation Myths with Aratus’s
      ‘Phaenomena’

-  `Fasti Capitolini <http://www.attalus.org/translate/fasti2.html>`__

-  `LacusCurtius: a large site on Roman antiquity
   <https://penelope.uchicago.edu/Thayer/E/Roman/home.html>`__

-  `The Historia Augusta
   <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Historia_Augusta/home.html>`__

-  `Res Gestae Divi Augusti
   <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Augustus/Res_Gestae/home.html>`__

-  `Aetna
   <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Aetna/home.html>`__

-  `Laus Pisonis
   <https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Laus_Pisonis/text*.html>`__

#########
 Studies
#########

-  [X] Figuri ilustre din Evul Mediu

-  Figuri ilustre din perioada renasterii

-  Jacques Le Goff

   -  [X] Imaginarul Medieval
   -  Civilizatia occidentului medieval

-  Mary Beard

   -  SQPR

-  Thomas Bulfinch

   -  Bulfinch's Mythology

-  Anthony Everitt

   -  [X] The Life and Times of Rome's Greatest Politician
   -  The Rise of Rome
   -  Alexander the Great
   -  SPQR

-  Bosworth Smith

   -  *Carthage and the Carthaginians*

-  Dexter Hoyos

   -  The Carthaginians

-  Guy Westwood

   -  [X] The Rhetoric of the Past in Demosthenes and Aeschines

-  Edwin Carawan

   -  The Attic Orators

-  Richard Claverhouse Jebb

   -  Attic Orators from Antiphon to Isaeos

-  Andreas Serafim

   -  Attic Oratory and Performance

-  Mircea Florian

   -  [X] Logica lui Aristotel (Prefata Organon)

-  D.M. Pippidi

   -  Formarea ideilor literare in antichitate

-  Heinrich Lausberg

   -  *Handbook of Literary Rhetoric*

-  Theodor Bergk

   -  *Poetae lyrici graeci*

-  Gerald Else

   -  *Aristotle's Poetics*

-  Istoria literaturii latine

-  Maria Marinescu—Himu

   -  Istoria literaturii eline

-  Jean—Pierre Vernant

   -  Mit si gandire in grecia antica
   -  Mit si religie in grecia antica

-  Dan Badarau

   -  *L’individuel chez Aristotel (1936)*

-  Frances Amelia Yates

   -  The Art of Memory

-  Kenneth J. Dover

   -  Greek Homosexuality

-  Josiah Ober

   -  Mass and Elite in Democratic Athens

-  Kurt Lampe

   -  [X] The Birth of Hedonism

-  Bettany Hughes

   -  The Hemlock Cup: Socrates, Athens, and the Search for the Good
      Life

-  Richard D. McKiraham

   -  Philosophy before Socrates

- Erwin Rohde

  - [X] Psyche

- Nicolae Sacalis-Calata

  - Filosofia si pedagogia culturii
    de la Homer la Platon si Zamolxe


