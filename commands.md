- print info about a movie such as embedded audio streams, subtitles, chapters, etc

  `HandBrakeCLI -i movie --scan`

- when you have a DVD folder with multiple video files and you want only main movie

  `HandBrakeCLI -i VIDEO_TS/ --main-feature -o vid.mkv`

- common conversion line
  - e -- encoder, nvidia h265
  - crop -- remove black bars
  - q -- quality
  - audio 2 aencoder -- select second audio from embedded streams and copy it
  - srt-file/lang/default -- select subtitle from file and import into film
  - subtitles/s -- select from embedded subtitles
  
   `HandBrakeCLI -i movie --main-feature -e nvenc_h265 --crop -q 22 --encoder-preset slowest --srt-file movie.srt --srt-lang en --srt-default=1 --audio 2 --aencoder copy -o vid2.mkv`
   
   `HandBrakeCLI -i VIDEO_TS/ --main-feature -e qsv_h265 --crop --slowest --subtitle 1 --audio 1,2 --aencoder copy -o out.mkv`

- get info about a specific encoder -- remove `list` to actually pass that option

  `--encoder-<preset/profile/tune/level>-list nvenc_h265`

- build emacs from source

  `./autogen.sh && ./configure --enable-link-time-optimization --with-x-toolkit=gtk3 --without-xaw3d --without-gsettings --without-compress-install --with-modules --without-libotf --without-m17n-flt --without-gconf --with-xinput2 --with-tree-sitter --prefix=$HOME/.local && make -j$(nproc --all)`

